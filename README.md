# markdown-css

Use `<link href="/home/marc/GitProjects/markdown-css/markdown.css" rel="stylesheet"></link>` on top of the markdown document  

Conversion with `pandoc -t html5 --metadata title="<title>" <file>.md -o <file>.pdf`

To test the html output:  
`pandoc <file>.md -o <file>.html`  

